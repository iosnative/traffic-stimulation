//
//  TypePickData.swift
//  Traffic Stimulation
//
//  Created by Rahul Kumawat on 17/11/21.
//

import Foundation

enum TypePickData: String {
    case clock = "Clockwise"
    case anticlock = "Anticlockwise"
    case stepupyoneC = "Step-Up by One - C"
    case stepdownoneC = "Step-Down by One - C"
    case stepupyoneA = "Step-Up by One - A"
    case stepdownoneA = "Step-Down by One - A"
}
