//
//  String.swift
//  Traffic Stimulation
//
//  Created by Rahul Kumawat on 17/11/21.
//

import Foundation

extension String {
    
    public var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
}
