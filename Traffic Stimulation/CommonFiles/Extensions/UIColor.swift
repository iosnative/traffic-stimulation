//
//  ColorsData.swift
//  Traffic Stimulation
//
//  Created by Rahul Kumawat on 17/11/21.
//

import Foundation
import UIKit

extension UIColor {
    static var redColor: UIColor  { return .systemPink }
    static var greenColor: UIColor { return .systemGreen }
    static var grayColor: UIColor { return .systemGray6 }
    static var blackColor: UIColor { return .lightGray }
}
