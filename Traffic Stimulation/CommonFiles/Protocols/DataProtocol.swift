//
//  DataProtocol.swift
//  Traffic Stimulation
//
//  Created by Rahul Kumawat on 17/11/21.
//

import Foundation

protocol DataProtocol {
    func getAllData(time: Int, mode: String, loop: Int)

}
