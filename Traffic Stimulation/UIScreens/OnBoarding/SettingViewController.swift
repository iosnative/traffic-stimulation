//
//  SettingViewController.swift
//  Traffic Stimulation
//
//  Created by Rahul Kumawat on 17/11/21.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var durationTextField: UITextField!
    @IBOutlet weak var modeTextField: UITextField!
    @IBOutlet weak var typePicker: UIPickerView!
    @IBOutlet weak var applySettingButton: UIButton!
    @IBOutlet weak var loopTextField: UITextField!
    
    var pickerData: [String] = [String]()
    var dataDelegate: DataProtocol?
    
    static var getInstance: SettingViewController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
        pickerData = [ TypePickData.clock.rawValue, TypePickData.anticlock.rawValue ]
        self.typePicker.delegate = self
        self.typePicker.dataSource = self
//        modeTextField.inputView = typePicker
        modeTextField.text = TypePickData.clock.rawValue
        applySettingButton.addTarget(self, action: #selector(didSelectApply), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @objc
    func didSelectApply() {
        var time = durationTextField.text
        let mode = modeTextField.text ?? TypePickData.clock.rawValue
        var loop = loopTextField.text
        let vc = ViewController.getInstance

        vc.modalPresentationStyle = .fullScreen
        if(time?.isEmpty == true){
            time = "1"
        }
        if(loop?.isEmpty == true){
            loop = "10"
        }
        if(time?.isNumber == true && loop?.isNumber == true) {
            let intTime = (time! as NSString).integerValue
            let intLoop = (loop! as NSString).integerValue
            self.dataDelegate?.getAllData(time: intTime, mode: mode, loop: intLoop)
            self.navigationController?.popViewController(animated: true)
        }
    }

}

extension SettingViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       modeTextField.text = pickerData[row]
    }
    
}
