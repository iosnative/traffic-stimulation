//
//  ViewController.swift
//  Traffic Stimulation
//
//  Created by Rahul Kumawat on 16/11/21.
//

import UIKit

class ViewController: UIViewController, DataProtocol{

    var customTrafficView = CustomTrafficView()
    
    @IBOutlet weak var view1: CustomTrafficView!
    @IBOutlet weak var view2: CustomTrafficView!
    @IBOutlet weak var view3: CustomTrafficView!
    @IBOutlet weak var view4: CustomTrafficView!
    @IBOutlet weak var modeButton: CustomModeView!
    @IBOutlet weak var settingButton: UIButton!
    // status
    @IBOutlet weak var currentLoopLabel: UILabel!
    @IBOutlet weak var finalLoopLabel: UILabel!
    @IBOutlet weak var intervalLabel: UILabel!
    @IBOutlet weak var modeLabl: UILabel!
    
    var timerBase: Timer? = nil
    var timerMain: Timer? = nil
    var timerSub: Timer? = nil
    var isStart: Bool = false
    var time: Int = 1
    var mode: String = TypePickData.clock.rawValue
    var loop: Int = 10
    var totalIndex: Int = 4
    var currentIndexOfButton = 0
    
    var views: [CustomTrafficView] = []
    var parentViews: [UIView] = []
    var startButtons: [UIButton] = []
    var alertViews: [UIView] = []
    var secLabels: [UILabel] = []
    
    static var getInstance: ViewController{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        intervalLabel.text = "\(time)"
        finalLoopLabel.text = "\(loop)"
        modeLabl.text = mode
        if(!isStart){
            modeButton.isHidden = true
        }
        views = [view1, view2, view3, view4]
        for index in 0..<totalIndex {
            self.parentViews.append(views[index].parentView)
            self.startButtons.append(views[index].startButton)
            views[index].startButton.tag = index
            self.alertViews.append(views[index].alertView)
            self.secLabels.append(views[index].secLabel)
        }
        settingButton.addTarget(self, action: #selector(didTapSettingButton), for: .touchUpInside)
        for index in 0..<totalIndex {
            startButtons[index].addTarget(self, action: #selector(fetchCurrentIndex), for: .touchUpInside)
        }
        modeButton.modeButton.addTarget(self, action: #selector(starterCalling), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    @objc func starterCalling() {
        isStart = false
        modeButton.isHidden = true
        initialState()
    }
    
    @objc func didTapSettingButton() {
        let vc = SettingViewController.getInstance
        vc.dataDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func fetchCurrentIndex(_ sender: UIButton) {
//        print(sender.tag)
        startStimulation(tag: sender.tag)
        self.pausTools()
    }
    
    func pausTools() {
        modeButton.isHidden = false
        self.isStart = true
        for index in 0..<totalIndex {
            self.startButtons[index].isEnabled = false
        }
    }
    
    func startStimulation(tag: Int) {
        var arrayTag = [[0,1,2,3],[1,2,3,0],[2,3,0,1],[3,0,1,2]]
        if(mode == TypePickData.anticlock.rawValue){
            arrayTag = [[0,3,2,1],[1,0,3,2],[2,1,0,3],[3,2,1,0]]
        }
        if(self.isStart){
            modeButton.isHidden = false
        }
        // start loop
        var ctb = 1
        self.changeBackground(indexPosition: ctb, tagPosition: arrayTag[tag])
        // 1st loop start
        ctb += 1
        self.timerBase = Timer.scheduledTimer(withTimeInterval: TimeInterval(self.time), repeats: true){ tb in
            if(ctb == 2){
                self.changeBackground(indexPosition: ctb, tagPosition: arrayTag[tag])
                print("2")
            }
            if(ctb == 3){
                self.changeBackground(indexPosition: ctb, tagPosition: arrayTag[tag])
                print("3")
            }
            if(ctb == 4){
                self.changeBackground(indexPosition: ctb, tagPosition: arrayTag[tag])
                print("4")
            }
            
//            if(ctb == 4){
//                ctb = 5
//            }
            if ctb == self.totalIndex {
                tb.invalidate()
                self.timerBase?.invalidate()
                print("END B")
                self.currentLoopLabel.text = "1"
            }
            ctb += 1
        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(self.time*2)) {
//            self.changeBackground(indexPosition: 4, tagPosition: arrayTag[tag])
//        }
        // 1st loop end
        // rmaaining loop start
        var ctm = 1
        timerMain = Timer.scheduledTimer(withTimeInterval: TimeInterval(self.time*4), repeats: true){ tm in
            print("LOOP Complete : ",ctm)
            var cts = 1
            self.changeBackground(indexPosition: cts, tagPosition: arrayTag[tag])
            cts += 1
            self.timerSub = Timer.scheduledTimer(withTimeInterval: TimeInterval(self.time), repeats: true){ ts in
//                if(cts == 1){
//                    self.changeBackground(indexPosition: cts, tagPosition: arrayTag[tag])
//                }
                if(cts == 2){
                    self.changeBackground(indexPosition: cts, tagPosition: arrayTag[tag])
                }
                if(cts == 3){
                    self.changeBackground(indexPosition: cts, tagPosition: arrayTag[tag])
                }
                if(cts == 4){
                    self.changeBackground(indexPosition: cts, tagPosition: arrayTag[tag])
                    self.currentLoopLabel.text = "\(ctm)"
                }
                if cts >= self.totalIndex {
                    ts.invalidate()
                    print("END S")
                }
                cts += 1
            }
            ctm += 1
            if ctm >= self.loop+1 {
                tm.invalidate()
                print("END M")
                self.initialState()
            }
        }
        // rmaaining loop end
    }
    
    func changeBackground(indexPosition:Int, tagPosition: [Int]) {
        for index in 0..<self.totalIndex {
            if(indexPosition-1 == index){
                self.parentViews[tagPosition[index]].backgroundColor = .lightGray
                self.alertViews[tagPosition[index]].backgroundColor = .greenColor
            }
            else{
                self.parentViews[tagPosition[index]].backgroundColor = .systemGray6
                self.alertViews[tagPosition[index]].backgroundColor = .redColor
            }
        }
    }
    
    func initialState() {
        timerSub?.invalidate()
        timerMain?.invalidate()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            for index in 0..<self.totalIndex {
                self.parentViews[index].backgroundColor = .systemGray6
                self.alertViews[index].backgroundColor = .redColor
                self.startButtons[index].isEnabled = true
                if(index == self.totalIndex-1){
                    self.isStart = false
                    self.modeButton.isHidden = true
                    self.currentLoopLabel.text = "0"
                }
            }
        }
    }
    
    func getAllData(time: Int, mode: String, loop: Int) {
        self.time = time
        self.mode = mode
        self.loop = loop
        for index in 0..<totalIndex {
            secLabels[index].text = "\(time)"
        }
        intervalLabel.text = "\(time)"
        finalLoopLabel.text = "\(loop)"
        modeLabl.text = mode
    }

}
