//
//  CustomTrafficView.swift
//  Traffic Stimulation
//
//  Created by Rahul Kumawat on 16/11/21.
//

import UIKit

class CustomTrafficView: UIView {
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var secLabel: UILabel!
    @IBOutlet weak var timingLabel: UILabel!
    
    let nibname = "CustomTrafficView"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else {return}
        view.frame = self.bounds
        self.addSubview(view)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func loadViewFromNib()-> UIView? {
        let nib = UINib(nibName: nibname, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil) .first as? UIView
    }
    
    func showView(view:UIView) {
        self.frame = view.bounds
        view.addSubview(self)
    }
}
